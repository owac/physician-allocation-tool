claim_table_basedata = reactive({

    # reactivity.
    data_objects$reactive_to('claim')
    input$sovtable_columnselector_click

    idt = query('select * from claim') %>% 
        filter(is.na(totrash)) %>%
        applyFilters() %>%
        # drop util columns.
        select(-created, -created_by, -modified, -modified_by, -totrash, -totrash_by, -end_date, -effective_date)

    idt = idt[, setdiff(names(idt), hidecols)]

    # names(idt) <- swap(names(idt))

    return(idt)
    
})

viz$add(
    id = 'claimtable',
    style = "max-width:100%; position:relative;",
    type = 'table', 
    title = '',
    subtitle = function(...) div(
        p("Click to edit claim surcharges."),
        div(
            style = 'margin-bottom: 10px; ',
            inline(actionButton(inputId = 'downloaddata_claim', label = 'Download Data'))
        ),
    ),
    mindim = c(1000, 500),
    data_download = claim_table_basedata,
    download_name = 'claim_surchage',
    outputfn = function(data_download) tablewithedit(
        data_download, 
        'claim', 
        options = list(scrollX = FALSE, filter = FALSE, info = FALSE, paging = FALSE, lengthChange = FALSE, initComplete = JS('fixtable_sovtable')),
        currency_cols = 'From|Through|Dollar Surcharge',
        pct_cols = 'Percent Surcharge'
))

claim_last_newdata = NULL # this observer triggers too often so we have to compare to prior button value.
observe(if(!is.null(input$newdata_claim)){
    if(!is.null(claim_last_newdata) && input$newdata_claim > claim_last_newdata){
        objectmodal(
            'claim', 
            'add',
            data = list(value_software = 0, value_edp_equip = 0)  # defaults on add.
        )
    }
   claim_last_newdata  <<- input$newdata_claim
})

observeEvent(input$downloaddata_claim, {
    updateTextInput(session = session, inputId = 'trigger_download', value = 'claimtable')
})