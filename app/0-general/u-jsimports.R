jsimports = function(){
    
    # get js files
    # deferred must be handled differently (added last).
    jsfiles = list.files('www/js', pattern = '[.]js$', recursive = TRUE)
    jsfiles_deferred = jsfiles[grepl('defer/', jsfiles)]
    jsfiles = setdiff(jsfiles, jsfiles_deferred)

    # some files require a specific order.
    # highcharts can be really tricky to get right. 
    # https://www.highcharts.com/forum/viewtopic.php?t=38587 
    # https://jsfiddle.net/c1tyag3f/
    jsorder = c(
      'external/highcharts/stock/highstock.js',
      'external/highcharts/stock/highcharts-more.js'
    )
    jsorder = c(jsorder, setdiff(jsfiles, jsorder))

    jsimports = lapply(jsorder, function(file) tags$script(src = glue('js/{file}')))
    
    # add deffered.
    for(file in jsfiles_deferred) jsimports[[length(jsimports) + 1]] <- tags$script(src = glue('js/{file}'))

    return(jsimports)

}
