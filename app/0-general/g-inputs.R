# functions for adding and tracking every kind of input.
# these should be used for any inputs that will be tracked with bookmarks.
# inputs that don't need to be saved should use regular input functions.
app_inputs = list(
    select = c(), num = c(), txt = c(), checkbox = c(), button = c(), daterange = c(),
    tabset = c('tabset', 'trend_tabset', 'act_tabset', 'cda_tabset', 'ndays_tabset')
)

# selectizeInput, selectInput
sel = function(id, choices = NULL, selected = NULL, label = NULL, multi = TRUE, width = '175px', islocked = FALSE){
    app_inputs$select <<- c(app_inputs$select, id)
    return(selectizeInput(
        inputId = id,
        label = label,
        choices = choices,
        multiple = multi,
        options = if(islocked){ list() } else if(multi){ list( plugins = list('remove_button'), placeholder = 'All' ) },
        selected = selected,
        width = width
    ))
}

# numericInput
num = function(id, label = NULL, min = NA, max = NA, step = 1, selected = NULL, width = 100){
    app_inputs$num <<- c(app_inputs$num, id)
    return(numericInput(inputId = id, label = label, width = width, min = min, max = max, step = step, value = selected))
}

# textInput
txt = function(id, label = NULL, selected = NULL, width = 150){
    app_inputs$txt <<- c(app_inputs$txt, id)
    return(textInput(
        inputId = id,
        label = label,
        width = width,
        value = selected
    ))
}

# checkBoxInput
checkbox = function(id, label = NULL, selected = FALSE){
    app_inputs$checkbox <<- c(app_inputs$checkbox, id)
    return(checkboxInput(
        inputId = id,
        label = label,
        value = as.logical(selected)
    ))
}

# buttonInput
button = function(id, label = NULL, icon = NULL, style = NULL, width = NULL, ...){
    app_inputs$button <<- c(app_inputs$button, id)
    return(actionButton( 
        inputId = id,
        label = label,
        icon = icon,
        style = style,
        width = width, 
        ...
    ))
}

# dateRange
daterange = function(id, label = NULL, icon = NULL, style = NULL, width = 250, selected = NULL, ...){
    app_inputs$daterange <<- c(app_inputs$daterange, id)
    if(is.null(selected)) selected = c(NA, NA)
    return(
        suppressWarnings( # unneccsary warning when selected is blank.
            dateRangeInput(
                inputId = id,
                label = label,
                separator = " through ",
                width = width,
                format = "mm/dd/yyyy",
                start = selected[1],
                end = selected[2],
                ...
    )))
}

