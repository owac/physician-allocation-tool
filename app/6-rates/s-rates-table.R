rates_table_reactive = reactive({

    proginit('Loading loss data.')
   
    idt = data$rates %>% 
        select(-rate_type) %>%
        mutate(rate_id = gsub("_", " ", rate_id),
               rate_id = tools::toTitleCase(rate_id))

    progclose()

    return(idt)

})

viz$add(
    id = 'rates_table',
    type = 'table',
    title = tagList(
        h1(class = 'inline', style = 'font-size: 18px; ', 'Rates')
    ),
    subtitle = '',
    filterid = '',
    mindim = c(445, 200),
    data_download = rates_table_reactive,
    outputfn = function(data_download) {

        if(is.null(data_download)) return(NULL)

        names(data_download) = swap(names(data_download))

        datatable(
            data_download,
            rownames = FALSE,
            options = list(
                paging = FALSE,
                info = FALSE,
                filter = FALSE                        
            )
        ) %>% 
            formatCurrency(
                columns = c('Rate'),
                digits = 0
            )

})