Different highcharts files with the same name aren't always the same, so this folder structure is set up to clarify where you should be pulling these files from.

In particular, highstocks requires specific versions of files from stock/

Some examples:

stock/modules/sankey.js should come from https://code.highcharts.com/stock/modules/sankey.js

highcharts-more.js should come from https://code.highcharts.com/highcharts-more.js

map/modules/map.js should come from https://code.highcharts.com/map/modules/map.js

