Shiny.addCustomMessageHandler('objectmodalerror', function(message){ 
    $("#objectmodal-error").removeClass("hidden").html(message);
});

function clickedit(infoboth){
    let info = infoboth.split('-');
    Shiny.onInputChange('clickedit', info);
}

function openworkbench(infoboth){
    let info = infoboth.split('-');
    Shiny.onInputChange('openworkbench', info);
}

function clickdelete(infoboth){
    let info = infoboth.split('-');
    Shiny.onInputChange('clickdelete', info);
}
