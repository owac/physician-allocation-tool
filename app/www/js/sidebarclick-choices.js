function sidebarclick(divid, inputid){
    Shiny.onInputChange( 'sidebarclick', [divid, inputid] );
}

// mimics R setdiff to remove one set from another:
function setdiff(x, remove) {
    return x.filter(function(el) {
        return remove.indexOf(el) < 0;
    });
}
function intersect(x, y){
    return x.filter(function(el) {
        return y.indexOf(el) >= 0;
    });
}

function updateoptions(divid, choices, selected){

    const selectize = $("#" + divid).find("select")[0].selectize;
    const keys = Object.keys(selectize.options);

    // drop all existing options so sort will reset.
    //const todrop = keys && keys.length > 0 ? setdiff(keys, choices) : [];
    const todrop = keys;
    $.each(todrop, function(i, x){ selectize.removeOption(x); });

    // add items not in choices.
    //const toadd = keys && keys.length > 0 ? setdiff(choices, keys) : choices;
    const toadd = choices;
    $.each(toadd, function(i, x){ selectize.addOption({value: x, label: x}) });

    // set selections.
    selectize.setValue(intersect(selected, choices));
    selectize.refreshOptions();
    
}
