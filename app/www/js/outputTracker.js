
$(document).ready(function() {
    // Using these as references:
    // https://owasp.org/www-project-secure-coding-practices-quick-reference-guide/stable-en/02-checklist/05-checklist
    function validateOutputs(outputID, outputValue)
    {

        // Check for event attributes in HTML output
        var hasEventAttributes = /on\w+\s*=/.test(outputValue)
       
        // Check that passing through as escaped and encoded HTML output
        var escapedProperly = /^\{html:\s*'.*'\}$/.test(outputValue);

        // Keeping check here to make sure all outputs are being passed through for a check
        // console.log("Checking: ", outputID, outputValue, hasEventAttributes, escapedProperly)

        // Validation check
        if (hasEventAttributes || escapedProperly)
        {
            Shiny.setInputValue(
                'validation_output_invalid', 
                {outputID, outputValue}, 
                {priority: "event"}
            );
        }

    }

    $(document).on("shiny:value", function(event) {
        var outputValue = event.value;
        try {
            var outputID = event.binding.binding.name;
        } catch (e) {
            var outputID = "Unknown Output";
        };
        validateOutputs(outputID, outputValue);
    });

});