/* 
    These functions are useful for formatting numbers in highcharter. To implement, use these or similar:
        dataLabels = list( formatter = JS('function(){ return chartLabel_val(this.y,1); }') )
        hc_yAxis( labels = list( y = 16, formatter = JS('function(){ return chartLabel_val(this.value, 1, 0); }') ) )
 */

// Code to format as number with commas. 
// https://beta.rstudioconnect.com/barbara/format-numbers/
function localeString(x, sep, grp) {
    var sx = (''+x).split('.'), s = '', i, j;
    sep || (sep = ',');            // default separator
    grp || grp === 0 || (grp = 3); // default grouping
    i = sx[0].length;
    while (i > grp) {
      j = i - grp;
      s = sep + sx[0].slice(j, i) + s;
      i = j;
    }
    s = sx[0].slice(0, i) + s;
    sx[0] = s;
    return sx.join('.');
}
// Functions for building chart labels. Mirrors similar functions in R.
function chartLabel(aspercent){    
    i = this.y ?  this.y : this.total;    
    return chartLabel_val( i, aspercent );
}
function chartLabel_val( ival, aspercent, digits, no_suffix ){
    
    i = ival;
    iabs = Math.abs(i);

    if( no_suffix ){
        var divby = 1;
        var isuffix = ''
    } else {
        var divby = iabs > 1000000000 ? 1000000000 : ( iabs > 1000000 ? 1000000 : ( iabs > 20000 ? 1000 : 1 ));
        var isuffix = iabs > 1000000000 ? 'B' : ( iabs > 1000000 ? 'M' : ( iabs > 20000 ? 'K' : '' ));
    }

    // Apply divby and convert to percentage.
    i = i / divby;
    if( aspercent ){
        i = i * 100;
        iabs = iabs * 100;
    }

    // Adjust digits if one wasn't passed.
    if( !digits && digits != 0 ){

        // Default.
        digits = 0;

        // If there are no digits, override digits to 0.
        if( Math.round(iabs) == iabs ){ digits = 0; }
        else if( iabs < 10 ){ digits = iabs < .1 ? 3 : 2; }

    }

    // Apply rounding.
    i = i.toFixed( digits );

    // Apply percent or commas and suffic.
    if( aspercent ){
        i = i + "%";
    } else {
        i = i.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        i = i + ( isuffix ? ' ' : '' ) + isuffix; 
    }
    
    return i;

}