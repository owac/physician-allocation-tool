
$(document).ready(function() {

    // Adding in a basic validation check function that will cover the following types of security threats
    // SQL Injection, Cross-Site Scripting, HTML Injection, Path Traversal, Code Injection, Command Injection
    // Using these as references:
    // https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html
    // https://owasp.org/www-project-secure-coding-practices-quick-reference-guide/stable-en/02-checklist/05-checklist
    function validateInputs(inputID, inputValue)
    {

        // SQL Injection
        if (inputValue.includes("; DROP TABLE") || inputValue.includes("--"))
        {
            Shiny.setInputValue(
                'validation_input_invalid', 
                {inputID, inputValue}, 
                {priority: "event"}
            );
        }

        // Cross-Site Scripting
        if (inputValue.includes("<script>") || inputValue.includes("</script>"))
        {
            Shiny.setInputValue(
                'validation_input_invalid', 
                {inputID, inputValue}, 
                {priority: "event"}
            );
        }

        // HTML Injection
        if (/<\/?[a-z][\sS]*>/i.test(inputValue))
        {
            Shiny.setInputValue(
                'validation_input_invalid', 
                {inputID, inputValue}, 
                {priority: "event"}
            );
        }

        // Path Traversal
        if (inputValue.includes("../") || inputValue.includes("..\\") || inputValue.includes("..%5c"))
        {
            Shiny.setInputValue(
                'validation_input_invalid', 
                {inputID, inputValue}, 
                {priority: "event"}
            );
        }

        // Code Injection
        if (inputValue.includes("$(document)."))
        {
            Shiny.setInputValue(
                'validation_input_invalid', 
                {inputID, inputValue}, 
                {priority: "event"}
            );
        }

        // Command Injection
        if (inputValue.includes(";") || inputValue.includes("&&") || inputValue.includes("|"))
        {
            Shiny.setInputValue(
                'validation_input_invalid', 
                {inputID, inputValue}, 
                {priority: "event"}
            );
        }

    }

    // Track general input changes
    $(document).on('input change', 'input:not([type="search"]), textarea, select', function() {
        var inputID = $(this).attr('id');
        var inputValue = $(this).val();

        validateInputs(inputID, inputValue)

    });

    // Track search bar changes
    $(document).on('input', 'input[type="search"]', function() {
        var inputID = "Search Bar";
        var inputValue = $(this).val();

        validateInputs(inputID, inputValue)

    })

    // Track Selectize Input changes
    $(document).on('change', 'selectized', function() {
        var inputID = $(this).attr('id');
        var selectizeInput = $(this)[0].selectize;
        var selectedValues = selectizeInput.getValue();

        validateInputs(inputID, selectedValues)

    });

});