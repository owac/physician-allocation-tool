// by default we disable this.
const doautozoom = false;
if( doautozoom ){

    var oldsize = -1;
    var zoom = 1;
    var origWidth;

    $( document ).ready(function() { // Run this code after all elements are loaded.

        // Auto-zoom to width.
            
            var elem = $('body');

            newsize = elem.width();
            origWidth = newsize;

            if( oldsize != newsize ) {
                zoom = Math.min( 100, Math.floor( $('body').width() / 1903 * 100 ) / 100 );
                elem.css('transform-origin', 'top left' );
                elem.css('transform', 'scale('+zoom+', '+zoom+')' );
                elem.css('width', '1900px' );
                oldsize = newsize;
            }   

        // Now that zoom is adjusted, add tooltip functionality. See https://jqueryui.com/tooltip/.
        // You'll also need to add this to new elements created via output if you want them to have tooltips.
        // Add more elements here if you want tooltips on them.
        $( 'p' ).tooltip();

        $(document).on( "tooltipopen", function( jevent, ui ) {

            //var top = Math.max( 50, $(ui.tooltip).offset().top * zoom );
            //var left = $(ui.tooltip).offset().left;

            // https://stackoverflow.com/questions/13040601/how-to-get-mouse-coordinate-after-transform
            //var top = jevent.clientY - document.body.scrollTop - document.documentElement.scrollTop;
            //var left = jevent.clientX - document.body.scrollLeft - document.documentElement.scrollLeft;

            var top = ( jevent.target.getBoundingClientRect().top + jevent.target.getBoundingClientRect().height ) / zoom;
            var left = ( jevent.target.getBoundingClientRect().left ) / zoom + 25;

            top = Math.ceil( top ) + 'px';
            left = Math.ceil( left ) + 'px';

            console.log('top: ' + top + '; left: ', left );

            $(ui.tooltip).css( 'position', 'absolute' );
            $(ui.tooltip).css( 'z-index', '99999' );
            $(ui.tooltip).css( 'top', top );
            $(ui.tooltip).css( 'left', left );

        } );

    });

    // This applies tooltips to any shiny visual changes in order to capture new built elements.
    $(document).on( 'shiny:visualchange', function( ielem ){

        // Add more elements here if you want tooltips on them.
        $(ielem.target).find( 'th' ).tooltip();

    });

}