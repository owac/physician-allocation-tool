# Adding a hidden text input for input validation and output validation
# Input updates when input validation is failed.
output$validation_logging_hidden <- renderUI(div(
    hidden(textInput(inputId = "validation_input_invalid", label = NULL)),
    hidden(textInput(inputId = "validation_output_invalid", label = NULL))
))

# Add a function to log information via rsyslog
log_event <- function(event_name, event_id, severity, context, msg) {

    # Early return if local environment. Only run on server.
    if(is_local) return()

    # Use session$clientData$url_hostname for server name.
    # Assign location and instance based on that.
    server_logging_config <- list(
        location = case_when(
            session$clientData$url_hostname == "shinydevint.myowg.com" ~ "usdf23v1092.mrshmc.com",
            session$clientData$url_hostname == "oliverwymanapps.mmc.com" ~ "USDFW21AS437.mrshmc.com"
        ),
        instance = case_when(
            session$clientData$url_hostname == "shinydevint.myowg.com" ~ paste0("https://shinydevint.myowg.com/", sub('.*data/', '', getwd())),
            session$clientData$url_hostname == "oliverwymanapps.mmc.com" ~ paste0("https://oliverwymanapps.mmc.com/", sub('.*shiny/', '', getwd()))
        )
    )

    log_event <- list(
        "Start" = "SIEM-APP",
        "Event name" = event_name,
        "Event ID" = event_id,
        "Event Type" = "Security",
        "Severity" = severity,
        "Timestamp" = format(Sys.time(), format = "%Y-%m-%dT%H:%M:%SZ"),
        "Client IP Address" = session$request$HTTP_X_FORWARDED_FOR,
        "Location" = server_logging_config$location,
        "Application Code" = "SHINR",
        "Application Component" = "R Shiny App",
        "Actor Name" = session$user,
        "Context" = context,
        "Msg" = msg,
        "Identity Provider" = "OWG Okta",
        "Application Instance" = server_logging_config$instance,
        "Acting As User" = session$user,
        "Authentication Channel" = "OWG Okta",
        "Tracking Number" = "",
        "Session ID" = session$token,
        "Correlation ID" = ""
    )
    
    log_event <- cc(log_event, sep = "|")
    
    rsyslog::open_syslog("Actuarial Shiny SIEM Logging")
    rsyslog::syslog(
        message = log_event,
        level = "INFO",
        facility = "LOCAL6"
    )
    rsyslog::close_syslog()
}

# Event 4624 - Login Success
# Pull in an observation if session starts.
observeEvent(session$clientData$url_search, {
    log_event("Login Success", 4624, "Info", "Login Success", paste0("User: ", session$user))
})

# Event 5060 - Input Validation Failure
# Pull in an observation if the validation input changes.
# Does not observe initially since is null.
observe({
    if (isval(input$validation_input_invalid)) {
        context <- paste0("Input Validation Failure for ID: ", input$validation_input_invalid$inputID)
        msg <- paste0("Failed input value: ", input$validation_input_invalid$inputValue)
        log_event("Input Validation Failure", 5060, "Error", context, msg)
    }
})

# Event 5060 - Output Validation Failure
# Pull in an observation if the validation output changes.
# Does not observe initially since is null.
observe({
    if (isval(input$validation_output_invalid)) {
        context <- paste0("Output Validation Failure for ID: ", input$validation_output_invalid$outputID)
        msg <- paste0("Failed output value: ", input$validation_output_invalid$outputValue)
        log_event("Output Validation Failure", 5060, "Error", context, msg)
    }
})

# Event 5060 - Session Management Exceptions
# Check for valid session data
# Check for invalid session data
observe({
    invalid_session <- c(
        is.null(session),
        is.na(session$clientData$url_protocol),
        is.na(session$clientData$url_hostname),
        is.na(session$clientData$url_port)
    )

    invalid_names <- names(invalid_session)[invalid_session]

    if (any(invalid_session)) {
        invalid_string <- paste(invalid_names, collapse = ", ")
        context <- paste0("Session Managment Exceptions: ", invalid_string)
        msg <- paste0("Session Managment Exceptions: ", invalid_string)
        log_event("Session Management Exceptions", 5060, "Error", context, msg)
    }
})

# Event 4634 - Sign Out
session$onSessionEnded(function() {
    log_event("Sign Out", 4634, "Info", "Sign Out", paste0("User: ", session$user))
})