# create a modal (pop-up) based on data objects.
objectmodal = function(tablename, add_edit, id = NULL, data = NULL){

    # save a log record.
    if(!is.null(id)) data_objects$log(table = tablename, desc = glue('open modal: {add_edit}'), id = id)
        
    dt = list()
    if(add_edit %in% c('edit', 'delete')){
        dt = as.list(query(glue('select * from {tablename} where id = ?'), list(id)))

        # If editing sov table, make sure to pull in prior records subset to the id of the entry that was clicked on.
        # Entries will be filled now.
        if(tablename == 'sov') {
          
            edit_id = id
            prior <- sovtable_corrections() %>% filter(id == edit_id) %>% mutate(id = sov_id)
            dt = as.list(prior)

        }

        if(tablename == 'facility') {
          
            edit_id = id
            prior <- facilitytable_corrections() %>% filter(id == edit_id) %>% mutate(id = facility_id)
            dt = as.list(prior)

        }

    } else if(add_edit == 'add' && !is.null(data)){
        dt = data
    }

   popup(
       title = if(add_edit == 'add'){
           'New Record'
       } else if(tablename == 'sov') {
           glue('{c("edit" = "Edit", "delete" = "Delete")[[add_edit]]} Physician Roster License Number: {dt$npi_number}')
       } else if(tablename == 'facility'){
           glue('{c("edit" = "Edit", "delete" = "Delete")[[add_edit]]} Facility Roster: {dt$facility_number}')
       } else if(tablename == 'claim'){
           glue('{c("edit" = "Edit", "delete" = "Delete")[[add_edit]]} Claim Surcharges: {dt$name}')
       } else {
           glue('{c("edit" = "Edit", "delete" = "Delete")[[add_edit]]} OW Database ID: {dt$id}')
       },
       confirmid = glue('{tablename}_{add_edit}_confirm'), 
       size = 'l',
       content = div(

        if(!is.null(data_objects[[tablename]]$info)) div(
            style = 'padding: 5px; background-color: #f2f2f2; margin-bottom: 10px; font-size: 10pt; ', 
            data_objects[[tablename]]$info
        ),

        # main fields.
        lapply(
            names(data_objects[[tablename]]$prototype),
            function(colname){

                inputId = glue('{tablename}_{add_edit}_{colname}')
                
                # get info from prototype.
                info = data_objects$colinfo(tablename, colname)
                class = info$class
                values = info$values
                
                label = swap(colname)
                label = gsub('[(][{]symbol[}][)]', '', label) # unique to global-payments-sov app: remove {sybmol}
                selected = dt[[colname]]

                dowidth = 270

                if(add_edit == 'delete'){
                    input = p(span(class = 'bold', style = glue('width: {dowidth}px; '), glue('{label}:')), br(), span(selected))
                } else {
                    
                    input = if(class %in% c('numeric', 'integer')){
                        numericInput(inputId = inputId, label = label, value = selected, width = dowidth)
                    } else if(class == 'logical'){
                        if(nanull(selected)) selected = data_objects[[tablename]]$prototype[[colname]][1]
                        if(nanull(selected)) selected = TRUE
                        checkboxInput(inputId = inputId, label = label, value = as.logical(selected), width = dowidth)
                    } else if(class == 'Date'){
                        suppressWarnings( # will warn with no concerns when value is NA.
                            dateInput(inputId = inputId, label = label, value = selected, width = dowidth)
                        )
                    } else if(!is.null(values)){
                        selectizeInput(inputId = inputId, label = label, choices = unique(c('', selected, values)), selected = selected, width = dowidth)
                    } else {
                        textInput(inputId = inputId, label = label, value = selected, width = dowidth)
                    }

                }

                return(inline(style = 'margin-top: 5px; margin-right: 10px; ', input))
                
            }
        ),

        # joined objects.
        if(!is.null(data_objects[[tablename]]$parents)) lapply(data_objects[[tablename]]$parents, function(jointable){
            if(jointable == 'subaccounts'){
                objects = query('
                    select 
                        s.id, 
                        a.name as account,
                        s.name as subaccount
                    from subaccounts s 
                    inner join accounts a 
                        on a.id = s.accounts_id and 
                        a.id = ?
                ', list(input$accounts_id))
                objects$name = cc(objects$account, objects$subaccount, sep = ' > ')
            } else if(jointable == 'accounts'){
                objects = query('select id, name from accounts where id = ?', list(input$accounts_id))
            } else {
                objects = query(glue('select id, name from {jointable}'))
            }
            choices = objects$id
            names(choices) = objects$name
            label = swap(glue('{jointable}_id'))
            inputId = glue('{tablename}_{add_edit}_{jointable}_id')
            return(div(class = 'mt9',
                selectizeInput(inputId = inputId, label = label, choices = choices, multiple = FALSE
            )))
        }),

        # many2many relationships.
        # this code was written for a specific app and is not generalized. if you use it, please generalize it so it'll work with any app.
        if(add_edit == 'edit' && tablename %in% data_objects$ids(many2many = TRUE)) lapply(data_objects$many2many_parent_for(tablename), function(many2many_tablename){

            otherparent = setdiff(data_objects[[many2many_tablename]]$parents, tablename)
            otherparent_infocols = names(data_objects[[otherparent]]$prototype)

            # right now this is only for accounts so we'll hard-code the join to account and the label.
            output[['editdata-current_map']] <<- crun("renderUI({
                data_objects$reactive_to('", many2many_tablename, "')
                t = c(input$xwalk_people_subaccounts_new, input$clickdelete, input$clickedit, input$clickcustomedit)
                dt = query('
                    select
                        xwalk.id,
                        otherparent.id as otherparent_id,
                        otherparent.name as subaccount,
                        a.name as account
                    from ", many2many_tablename, " as xwalk
                    inner join ", otherparent, " as otherparent 
                        on otherparent.id = xwalk.", otherparent, "_id and 
                        xwalk.", tablename, "_id = ", id, "
                    inner join accounts as a on otherparent.accounts_id = a.id
                ')
                dt %<>% 
                    mutate(subaccount = cc(dt$account, dt$subaccount, sep = ' > ')) %>%
                    select(-account)
                return(tablewithedit(dt, '", many2many_tablename, "', delete = TRUE, deletename = 'Unlink'))
            })")
            
            # choices for adding new relationships.
            choices_dt = if(isolate(input$viewby == 'Account')){
                query(glue('
                    select 
                        t1.id, 
                        t1.name as subaccount,
                        a.name as account
                    from {otherparent} as t1
                    inner join accounts as a 
                        on t1.accounts_id = a.id and 
                        a.id = ?
                '), list(input$accounts_id))
            } else {
                query(glue('
                    select 
                        t1.id, 
                        t1.name as subaccount,
                        a.name as account
                    from {otherparent} as t1
                    inner join accounts as a 
                        on t1.accounts_id = a.id
                '))
            }
            choices = choices_dt$id
            names(choices) = cc(choices_dt$account, choices_dt$subaccount, sep = " > ")

            return(div(
                div( class = 'xwalk-select',
                    inline(
                        selectizeInput(inputId = glue('{many2many_tablename}_new_otherparent_id'), label = swap(many2many_tablename), choices = choices, multiple = FALSE)
                    ),
                    inline(class = 'modal-new', actionButton(inputId = glue('{many2many_tablename}_new'), label = '+'))
                ),
                p(class = 'warning', 'Undo button is applied immediately.'),
                uiOutput('editdata-current_map')
            ))

        }),        

        p(id = 'objectmodal-error', class = 'hidden objectmodalerror')

    ))
}

# open an edit modal when an edit cell is clicked.
last_click_edit = list()
observe({ if(isval(input$clickedit)){
    tablename = sqlvalcheck(input$clickedit[1])
    id = sqlvalcheck(input$clickedit[2])
    last_click_edit[[tablename]] <<- id
    objectmodal(tablename, 'edit', id)
    updateTextInput(session = session, inputId = 'clickedit', value = '')
}})

# navigate to workbench when edit modal is clicked.
openworkbench_lastclick = list()
observe({ if(isval(input$openworkbench) && input$openworkbench[1] != 'clear'){

    # extract info from JS.
    table = c('sov' = 'Physician Roster', 'facility' = 'Facility')[[
        input$openworkbench[1]
    ]]    
    donum = input$openworkbench[2]

    openworkbench_lastclick <<- list(table = table, donum = donum)

    # move to workbench and autoselect number, table.
    updateTabsetPanel(session = session, inputId = "tabset", selected = "workbench")
    updateSelectizeInput(session = session, inputId = 'workbench_table_select', selected = table)
    updateTextInput(session = session, inputId = 'workbench_id_text', value = donum)
    
    # click Search
    runjs('$("#workbench_id_search").click();')

    # clear openworkbench so the next click on the same item will work.
    updateTextInput(session = session, inputId = 'openworkbench', value = 'clear')
    
}})

# open an delete modal when an edit cell is clicked.
last_click_delete = list()
observe({ if(isval(input$clickdelete)){
    tablename = sqlvalcheck(input$clickdelete[1])
    id = sqlvalcheck(input$clickdelete[2])
    last_click_delete[[tablename]] <<- id
    objectmodal(tablename, 'delete', id)
    updateTextInput(session = session, inputId = 'clickdelete', value = '')
}})

# generic popup.
# https://shiny.rstudio.com/reference/shiny/latest/modalDialog.html
popup = function(title = 'Alert', content, confirmid = 'modalconfirm' , cancelbutton = TRUE, ...) showModal(modalDialog(
  title = title,
  content,
  ...,
  footer = tagList(
    if(cancelbutton) modalButton("Cancel"),
    actionButton(confirmid, "Continue")
  )
))
observeEvent(input$modalconfirm, removeModal())

# add inputs to UI so they can be cleared for repeat clicks.
# !!! MUST ADD uiOutput('data_objects_modal') TO ui.R
output$data_objects_modal = renderUI({ div( class = 'hidden',
    textInput(inputId = 'clickedit', label = NULL),
    textInput(inputId = 'clickcustomedit', label = NULL),
    textInput(inputId = 'clickdelete', label = NULL)
)})

stdtable = function(x) if(isval(x)) return(x)
tablewithedit = function(x, tablename, edit = TRUE, delete = FALSE, deletename = 'Delete', num_comma_cols = NULL, currency_cols = NULL, pct_cols = NULL, date_cols = NULL, keepid = FALSE, customedit = FALSE, makecorrections = FALSE, ...) if(isval(x)){

    if('id' %ni% names(x)) stop('
        Column [id] must be passed to tablewithedit(). Please add it. Error E1112.
    ')

    if(is.character(x)) stop(cc(tablename, ': ', x))
    if(nrow(x) > 0){
        if(edit) {
            if(customedit) {
                x$Edit = cc('<i class="far fa-edit clickable" onclick="clickcustomedit(\'', tablename, '-', x$id, '\')"/>')
            } else {
                x$Edit = cc('<i class="far fa-edit clickable" onclick="clickedit(\'', tablename, '-', x$id, '\')"/>')
            }
        }
        if(delete){
            # don't allow undo if there is only one record.
            # otherwise, users may disappear and be hard to find again (not linked to any account).
            x[[deletename]] = cc('<i class="fas fa-trash clickable " onclick="clickdelete(\'', tablename, '-', x$id, '\')"/>')
        }
        if(tablename == 'subaccounts') x$Edit[ x$Name == 'Master' ] <- ''
    }
    
    x = x[ , setdiff(names(x), c('otherparent_id', if(!keepid) 'id'))]
    names(x) = swap(names(x), oneway = TRUE)

    # bring add/edit first.
    firstcols = intersect(c('Edit', 'Delete'), colnames(x))
    x = x[, c(firstcols, setdiff(colnames(x), firstcols))]

    # handle column formats. for now, assume we use regex to identify columns.  
    for(iarg in c('num_comma_cols', 'currency_cols', 'pct_cols', 'date_cols')) crun(
        'if(!is.null(', iarg, ')) ', iarg, ' = grep("', get(iarg), '", colnames(x))'
    )

    if(makecorrections) x %<>% rename(`Make corrections` = Edit)
    
    return(
        datatable(
            x,
            escape = FALSE,
            rownames = FALSE,
            ...
        ) %>%
            `if`( length(num_comma_cols) > 0, formatCurrency(., columns = num_comma_cols, currency = '', digits = 0 ), . )  %>% 
            `if`( length(currency_cols) > 0, formatCurrency(., columns = currency_cols, digits = 0 ), . )  %>% 
            `if`( length(pct_cols) > 0, formatPercentage(., columns = pct_cols), . )  %>% 
            `if`( length(date_cols) > 0, formatDate(., columns = date_cols), . )
    )

}

tablehtml = function(x){
    
    if(nrow(x) == 0) return(p('No data'))
    
    tags$table(#class = 'hidden',
        tags$thead(tags$tr(
            lapply(names(x), function(x) tags$th(style = 'padding: 3px; ', x))
        )),
        tags$tbody(
            lapply(1:nrow(x), function(row) tags$tr(
                lapply(1:ncol(x), function(col) tags$td(
                    style = 'padding: 3px; ', 
                    HTML(x[row, col])
                ))
            ))
        )
    )

}
